package me.bumblebeee_.assesment;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import me.bumblebeee_.assesment.events.RegisterEvents;
import net.md_5.bungee.api.ChatColor;

public class Assesment extends JavaPlugin {
	
	Sql sql = new Sql();
	
	public void onEnable() {
		sql.setup();
		RegisterEvents.setup();
	}
	
	public void onDisable() {
		sql.disconnect();
	}
	
	private static Assesment instance;

    public Assesment() {
        instance = this;
    }

    public static Assesment getInstance() {
        return instance;
    }

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		
		if (cmd.getName().equalsIgnoreCase("statistics")) {
			
			if (args.length > 1) {
				sender.sendMessage(ChatColor.RED + "Invalid arguments. Correct usage: /stats <player>");
				return true;
			}
			
			int placed = 0;
			int broken = 0;
			int kills = 0;
			int deaths = 0;
			int food = 0;
			String name = "";
			
			if (args.length == 0) {
				if (!(sender instanceof Player)) {
					sender.sendMessage(ChatColor.RED + "Invalid arguments. Correct usage: /stats <player>");
					return true;
				}
				
				Player p = (Player) sender;
				
				try {
					placed = Integer.parseInt(sql.get("Placed", "Statistics", p.getUniqueId().toString()));
					broken = Integer.parseInt(sql.get("Broken", "Statistics", p.getUniqueId().toString()));
					kills = Integer.parseInt(sql.get("Kills", "Statistics", p.getUniqueId().toString()));
					deaths = Integer.parseInt(sql.get("Deaths", "Statistics", p.getUniqueId().toString()));
					food = Integer.parseInt(sql.get("Food", "Statistics", p.getUniqueId().toString()));
					name = p.getName();
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				Player t = Bukkit.getServer().getPlayer(args[0]);
				if (t == null) {
					OfflinePlayer ot = Bukkit.getServer().getOfflinePlayer(args[0]);
					try {
						placed = Integer.parseInt(sql.get("Placed", "Statistics", ot.getUniqueId().toString()));
						broken = Integer.parseInt(sql.get("Broken", "Statistics", ot.getUniqueId().toString()));
						kills = Integer.parseInt(sql.get("Kills", "Statistics", ot.getUniqueId().toString()));
						deaths = Integer.parseInt(sql.get("Deaths", "Statistics", ot.getUniqueId().toString()));
						food = Integer.parseInt(sql.get("Food", "Statistics", ot.getUniqueId().toString()));
						name = ot.getName();
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					try {
						placed = Integer.parseInt(sql.get("Placed", "Statistics", t.getUniqueId().toString()));
						broken = Integer.parseInt(sql.get("Broken", "Statistics", t.getUniqueId().toString()));
						kills = Integer.parseInt(sql.get("Kills", "Statistics", t.getUniqueId().toString()));
						deaths = Integer.parseInt(sql.get("Deaths", "Statistics", t.getUniqueId().toString()));
						food = Integer.parseInt(sql.get("Food", "Statistics", t.getUniqueId().toString()));
						name = t.getName();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			
			int kd = kills / deaths;
			
			sender.sendMessage(ChatColor.GOLD + "--------------------");
			sender.sendMessage(ChatColor.GOLD + "Name: " + name);
			sender.sendMessage(ChatColor.GOLD + "Placed Blocks: " + placed);
			sender.sendMessage(ChatColor.GOLD + "Broken Blocks " + broken);
			sender.sendMessage(ChatColor.GOLD + "Kills: " + kills);
			sender.sendMessage(ChatColor.GOLD + "Deaths: " + deaths);
			sender.sendMessage(ChatColor.GOLD + "Food Consumed: " + food);
			sender.sendMessage(ChatColor.GOLD + "K/D Ratio: " + kd);
			sender.sendMessage(ChatColor.GOLD + "--------------------");
			
			
		}
		
		return true;
	}

}
