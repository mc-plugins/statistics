package me.bumblebeee_.assesment;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

public class Sql {
	
	static Connection con = null;
	public void setup() {
		String host = Assesment.getInstance().getConfig().getString("mysql.host");
		String port = Assesment.getInstance().getConfig().getString("mysql.port");
		String db = Assesment.getInstance().getConfig().getString("mysql.database");
		String user = Assesment.getInstance().getConfig().getString("mysql.user");
		String pass = Assesment.getInstance().getConfig().getString("mysql.pass");
		
		try {
			con = getConn(host, port, user, pass, db);
			createTables();
		} catch (Exception e) {
			Bukkit.getLogger().warning("[Statistics] Could not connect to MySQL!");
			e.printStackTrace();
		}
	}
	
	public void disconnect() {
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public String get(String colum, String table, String uuid) throws Exception {
		
		ResultSet result = null;
		PreparedStatement statement = null;
		String data = "";
		
		try {
			statement = con.prepareStatement("SELECT " + colum + " FROM " + table + " WHERE UUID='" + uuid + "'");
			
			result = statement.executeQuery();
			
			if (result.next()) {
				data = result.getString(colum);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		    try { result.close(); } catch (Exception e) { /* ignored */ }
		    try { statement.close(); } catch (Exception e) { /* ignored */ }
		}
		return data;
	}
	
	public void death(final UUID uuid, final UUID killer) {
		new BukkitRunnable() {
	           
            @Override
            public void run() {
                try {
                	int deaths = Integer.parseInt(get("Deaths", "Statistics", uuid.toString())) + 1;
                    PreparedStatement s = con.prepareStatement("UPDATE Statistics SET Deaths=" + deaths + " WHERE UUID='" + uuid + "'");
                    s.execute();
                    s.close();
                    
                    
                    int kills = Integer.parseInt(get("Kills", "Statistics", killer.toString())) + 1;
                    PreparedStatement s2 = con.prepareStatement("UPDATE Statistics SET Kills=" + kills + " WHERE UUID='" + killer + "'");
                    s2.execute();
                    s2.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Assesment.getInstance());
	}

	public void consume(final UUID uuid) {
		new BukkitRunnable() {
	           
            @Override
            public void run() {
                try {
                	int consumed = Integer.parseInt(get("Food", "Statistics", uuid.toString())) + 1;
                    PreparedStatement s = con.prepareStatement("UPDATE Statistics SET Food=" + consumed + " WHERE UUID='" + uuid + "'");
                    s.execute();
                    s.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Assesment.getInstance());
	}
	
	public void blockBreak(final UUID uuid) {
		new BukkitRunnable() {
	           
            @Override
            public void run() {
                try {
                	int broken = Integer.parseInt(get("Broken", "Statistics", uuid.toString())) + 1;
                    PreparedStatement s = con.prepareStatement("UPDATE Statistics SET Broken=" + broken + " WHERE UUID='" + uuid + "'");
                    s.execute();
                    s.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Assesment.getInstance());
	}
	
	public void blockPlace(final UUID uuid) {
		new BukkitRunnable() {
	           
            @Override
            public void run() {
                try {
                	int placed = Integer.parseInt(get("Placed", "Statistics", uuid.toString())) + 1;
                    PreparedStatement s = con.prepareStatement("UPDATE Statistics SET Placed=" + placed + " WHERE UUID='" + uuid + "'");
                    s.execute();
                    s.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Assesment.getInstance());
	}
	
	public boolean playerExists(final UUID uuid) {
		boolean exists = false;
        try {
            PreparedStatement s = con.prepareStatement("SELECT UUID FROM Statistics WHERE UUID='" + uuid + "'");
            ResultSet r = s.executeQuery();
            if (r.next()) {
            	s.close();
            	exists = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
		return exists;
	}
	
	public void addPlayer(final UUID uuid) {
		new BukkitRunnable() {
	           
            @Override
            public void run() {
                try {
                	if (!playerExists(uuid))  {
                		PreparedStatement s = con.prepareStatement("INSERT INTO Statistics (UUID, Placed, Broken, Kills, Deaths, Food, KD)  VALUES ('" + uuid + "',0 ,0, 0, 0, 0, 0.0)");
		                s.execute();
		                s.close();
                	}
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Assesment.getInstance());
	}
	
	public void createTables() throws Exception {
		
		PreparedStatement table = null;
		
		try {
			
			table = con.prepareStatement("CREATE TABLE IF NOT EXISTS Statistics(UUID varchar(255), Placed integer, Broken integer, Kills integer, Deaths integer, Food integer, KD integer, PRIMARY KEY(UUID))");			
			
			table.executeUpdate();
			
		} catch (Exception e) {
			Bukkit.getServer().getLogger().severe("[Statistics] Error creating tables!");
			e.printStackTrace();
		} finally {
		    try { table.close(); } catch (Exception e) { /* ignored */ }
		}
		
	}
	
	
	
	public Connection getConn(String host, String port, String user, String pass, String database) throws Exception {
		
		try {
			String driver = "com.mysql.jdbc.Driver";
			String url = "jdbc:mysql://" + host + ":" + port + "/" + database + "?user=" + user + "&password=" + pass;
			Class.forName(driver);
			
			Connection conn = DriverManager.getConnection(url, user, pass);
			return conn;
		} catch (Exception e) {
			Bukkit.getServer().getLogger().severe("[Statistics] Error connecting to database!");
			e.printStackTrace();
		}
		
		return null;
	}

}
