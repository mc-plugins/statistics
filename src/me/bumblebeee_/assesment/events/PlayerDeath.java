package me.bumblebeee_.assesment.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import me.bumblebeee_.assesment.Sql;

public class PlayerDeath implements Listener {
	
	Sql sql = new Sql();
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent e) {
		if (e.getEntity() instanceof Player && e.getEntity().getKiller() instanceof Player) {
			sql.death(e.getEntity().getUniqueId(), e.getEntity().getKiller().getUniqueId());
		}
	}

}
