package me.bumblebeee_.assesment.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import me.bumblebeee_.assesment.Sql;

public class PlayerLogin implements Listener {
	
	Sql sql = new Sql();
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {
		sql.addPlayer(e.getPlayer().getUniqueId());
	}

}
