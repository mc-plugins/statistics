package me.bumblebeee_.assesment.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemConsumeEvent;

import me.bumblebeee_.assesment.Sql;

public class FoodConsume implements Listener {
	
	Sql sql = new Sql();
	
	@EventHandler
	public void onConsume(PlayerItemConsumeEvent e) {
		if (e.getItem().getType().isEdible()) {
			sql.consume(e.getPlayer().getUniqueId());
		}
	}
}
