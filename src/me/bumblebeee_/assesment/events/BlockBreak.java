package me.bumblebeee_.assesment.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import me.bumblebeee_.assesment.Sql;

public class BlockBreak implements Listener {
	
	Sql sql = new Sql();
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e) {
		sql.blockBreak(e.getPlayer().getUniqueId());
	}

}
