package me.bumblebeee_.assesment.events;

import org.bukkit.Bukkit;

import me.bumblebeee_.assesment.Assesment;

public class RegisterEvents {
	
	public static void setup() {
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerLogin(), Assesment.getInstance());
		Bukkit.getServer().getPluginManager().registerEvents(new BlockPlace(), Assesment.getInstance());
		Bukkit.getServer().getPluginManager().registerEvents(new BlockBreak(), Assesment.getInstance());
		Bukkit.getServer().getPluginManager().registerEvents(new FoodConsume(), Assesment.getInstance());
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerDeath(), Assesment.getInstance());
	}

}
